package com.tsopic.leapfisher.Tasks;

import org.powerbot.script.methods.Hud.Window;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.Item;
import com.tsopic.leapfisher.Fisher.Leapfisher;



public class Drop extends Task {
	int[] fishID;


	public Drop(MethodContext ctx, int[] fishID)  {
		super(ctx);
		this.fishID=fishID;
	}

	@Override
	public boolean activate() {

		return ctx.backpack.count()==28;
	}

	@Override
	public void execute() {
		com.tsopic.leapfisher.Fisher.Leapfisher.Status="Drop";
		//ItemQuery<Item> query= ctx.backpack.select();
		int count=0;
		int lenght= ctx.backpack.id(fishID).count();

		do{
			ctx.hud.view(Window.BACKPACK);

			for (Item i : ctx.backpack.id(fishID)) {
				i.interact("Drop");
				count++;
			}
		}while (ctx.backpack.select().id(fishID).count() > 0 && count<lenght);

		//System.out.println(a);

	}

}