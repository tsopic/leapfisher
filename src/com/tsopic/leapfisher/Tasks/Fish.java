package com.tsopic.leapfisher.Tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.util.Condition;
import org.powerbot.script.wrappers.Npc;
import org.powerbot.script.wrappers.Player;
import com.tsopic.leapfisher.Fisher.Leapfisher;



public class Fish extends Task{
	public Leapfisher leapfisher;
	private int fishingSpotID;
	private String action="Use-rod";



	public Fish(MethodContext ctx, int id){
		super(ctx);
		fishingSpotID= id;

	}

	@Override
	public boolean activate() {
		final Player myPlayer = ctx.players.local();
		final Npc fishingspot = ctx.npcs.select().id(fishingSpotID).nearest().peek();
  
			return fishingspot.isValid()&&ctx.backpack.select().count()<28&&myPlayer.isIdle();


	}

	@Override
	public void execute() {
		Leapfisher.Status="Fishing....";
		final Npc fishingspot = ctx.npcs.select().id(fishingSpotID).nearest().peek();
		if(fishingspot.isValid()){
			if(fishingspot.isInViewport()){
				if(fishingspot.interact(action)){
					Condition.wait(new Callable<Boolean>() {

						@Override
						public Boolean call() throws Exception {
							return ctx.players.local().isIdle();
						}
					}, 500, 4);
				}

			}else{
				if(ctx.movement.stepTowards(fishingspot.getLocation())){
					Condition.wait(new Callable<Boolean>() {
						public Boolean call() throws Exception{
							return ctx.players.local().isInMotion();
						}
					}, 500, 5);
				}
			}
		}

	}

}