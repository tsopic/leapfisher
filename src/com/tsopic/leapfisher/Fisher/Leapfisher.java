package com.tsopic.leapfisher.Fisher;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.powerbot.event.MessageEvent;
import org.powerbot.event.MessageListener;
import org.powerbot.event.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Manifest;
import org.powerbot.script.methods.Skills;
import org.powerbot.script.util.Timer;
import org.powerbot.script.wrappers.Npc;

import com.tsopic.leapfisher.Tasks.Drop;
import com.tsopic.leapfisher.Tasks.Fish;
import com.tsopic.leapfisher.Tasks.Task;

@SuppressWarnings("deprecation")
@Manifest(description = "Leapfisher", name = "Leapfisher", authors = "Tsopic")
public class Leapfisher extends PollingScript implements PaintListener, MessageListener{

	public List<Task> tasks = new ArrayList<>();
	public final int fishingSpotID=2722;
	public final int[] fishID = {11330,11332, 11328};
	public final int[] baitID = {313, 11334};
	public double fishingXP;
	public int fishingLVL;
	public static String Status = "Starting..";
	Map<String, Integer> fishCount = new HashMap<String, Integer>();
	public Npc Spotmodel = null;
	
	
	public void start(){
			tasks.add(new Fish(ctx, fishingSpotID));
			tasks.add(new Drop(ctx, fishID));
			addFishesToMap();
			fishingXP = ctx.skills.getExperience(Skills.FISHING);
			fishingLVL = ctx.skills.getLevel(Skills.FISHING);
			
	}
	
	@Override
	public int poll() {
			for(Task task : tasks){
				if(task.activate()){
					task.execute();
				}
			}
			return 100;
	}
	
	public void messaged(MessageEvent e) {
        String msg = e.getMessage().toLowerCase();
        if (msg.contains("trout")) {
            fishCount.put("trout", fishCount.get("trout")+1);
        }
        if (msg.contains("salmon")) {
            fishCount.put("salmon", fishCount.get("salmon")+1);
           
        }
        if (msg.contains("sturgeon")) { 
            fishCount.put("sturgeon", fishCount.get("sturgeon")+1);
        }
      
        
    }
	
	@SuppressWarnings("deprecation")
	public void repaint(Graphics g){
		
		long runtime = getRuntime();
		int gainedFishingLVL = ctx.skills.getLevel(Skills.FISHING)-fishingLVL;
		// First Row
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(5, 455, 250, 120);
		g.setColor(Color.BLACK);
		g.drawString("Runtime: " + Timer.format(runtime), 10, 475);
		g.setColor(Color.BLUE);
		g.drawString("Status: " + Status, 10, 490);
		g.drawString("Xp/h: "+ Math.round(perHour()) , 10, 505);
		g.setColor(Color.yellow);
		g.drawString("Sturgeon: "+ fishCount.get("sturgeon") , 10, 520);
		g.drawString("Trout: "+ fishCount.get("trout") , 10, 535);
		g.drawString("Salmon: "+ fishCount.get("salmon") , 10, 550);
		// Second Row of paint
		g.drawString("Fish caught: " + totalFish(), 130, 475);
		g.drawString("xp Earned: " + totalXP() + "("+ gainedFishingLVL +")", 130, 490);
	/*	if(Spotmodel.isValid()&&Spotmodel!=null){
			Spotmodel.draw(g);
		}*/
		
	}
	
	private double perHour(){ 
		double xp = ctx.skills.getExperience(Skills.FISHING)-fishingXP;
		return xp/getRuntime()*3600000D;
		
	}
	
	private String totalXP(){
		return Double.toString(ctx.skills.getExperience(Skills.FISHING)-fishingXP);
	}
	
	private String totalFish(){
		return Integer.toString(Math.round(fishCount.get("sturgeon")+fishCount.get("trout")+fishCount.get("salmon")));
	}
	
	private void addFishesToMap(){
		fishCount.put("sturgeon",0);
		fishCount.put("salmon", 0);
		fishCount.put("trout", 0);
	}
	
	public int[] getBaitID(){
		return baitID;
	}
	
	public int[] getFishID(){
		return fishID;
	}

}